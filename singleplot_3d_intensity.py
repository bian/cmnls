import numpy as np
from numpy import ma
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable

from mpl_toolkits.mplot3d import axes3d
import sys

import plot_cl as pcl
import rplot_edensity as rped
import rplot_intensity as rpin

def main():

    arglen=len(sys.argv)
    if arglen<2:
        print 'no specific dest file'
        exit()

    fig = plt.figure()
    ax = plt.gca(projection='3d')#fig.add_subplot(1, 1, 1)

    assert arglen-1 == 1

    X, Y, intens = rpin.parse_from_file(sys.argv[1])
    surf = pcl.plot_3d_intensity(ax, X, Y, intens)
    plt.colorbar(surf)
    
    ax.set_xlabel('distance(um)')
    ax.set_ylabel('radius(um)')
    plt.show()

if __name__ == '__main__':
    main()
