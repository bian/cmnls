#!/usr/bin/env python
# -*- coding: utf-8 -*-



import wx
import sys,math,os
import threading
import shlex, subprocess
import time,datetime
import wx.lib.mixins.listctrl as listmix

import wx.lib.agw.ultimatelistctrl as ULC
import pipe

class taskItem:
    def __init__(self,cfgfileName):
        self.startTime = datetime.datetime.now()
        
        #still need to count the time?
        self.bCount=True
        self.progressValue=0
        self.title=cfgfileName
        self.UpdateTime()
        #create and start thread
        self.wthread = None
    
    def _reset_starttime(self):
        self.startTime = datetime.datetime.now()
    def __getFormatedElapsedTime(self):
        nowTime = datetime.datetime.now()
        elapsedTime = nowTime - self.startTime
        secondsSum=elapsedTime.total_seconds()
        minutesSum=secondsSum/60
        return "%d minutes %d seconds" % (minutesSum,secondsSum % 60)
    
    def restart(self):
        self._reset_starttime()
        self.wthread = workThread(self,self.title)
        self.wthread.start()
    def UpdateTime(self):
        self.time = self.__getFormatedElapsedTime()
    def UpdateDoneFlag(self, bdone=True):
        if bdone:
            self.time = "%s -finished" % self.__getFormatedElapsedTime()
        else:
            self.UpdateTime()
        
PIPE_HEIGHT = 18
PIPE_WIDTH = 2000
class taskRenderer(object,taskItem):

    DONE_BITMAP = None
    REMAINING_BITMAP = None

    def __init__(self, parent,cfgName):

        #self.progressValue = 23
        #self.title="Cfg Name"
        #self.time="time"
        self.index=-1
        self.Updater = None
        taskItem.__init__(self,cfgName)
    def DrawSubItem(self, dc, rect,line,  highlighted, enabled):
        """Draw a custom progress bar using double buffering to prevent flicker"""

        canvas = wx.EmptyBitmap(rect.width, rect.height)
        mdc = wx.MemoryDC()
        mdc.SelectObject(canvas)

        if highlighted:
            mdc.SetBackground(wx.Brush(wx.SystemSettings_GetColour(wx.SYS_COLOUR_HIGHLIGHT)))
            mdc.SetTextForeground(wx.WHITE)
        else:
            mdc.SetBackground(wx.Brush(wx.SystemSettings_GetColour(wx.SYS_COLOUR_WINDOW)))
        mdc.Clear()

        mdc.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, False))

        ypos = 5
        xtext, ytext = mdc.GetTextExtent(self.title)
        mdc.DrawText(self.title, 0, ypos)
        ypos += ytext + 5
        
        mdc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL, False))

        xtext, ytext = mdc.GetTextExtent(self.time)
        mdc.DrawText(self.time, 0, ypos)
        ypos += ytext + 5

        self.DrawProgressBar(mdc, 0, ypos, rect.width, 20, self.progressValue)
        
        dc.Blit(rect.x+3, rect.y, rect.width-6, rect.height, mdc, 0, 0)

    def GetLineHeight(self):

        dc = wx.MemoryDC()
        dc.SelectObject(wx.EmptyBitmap(1, 1))
        dc.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, False))
        dummy, ytext1 = dc.GetTextExtent("Agw")
        dc.SetFont(wx.Font(7, wx.SWISS, wx.NORMAL, wx.NORMAL, False))
        dummy, ytext2 = dc.GetTextExtent("Agw")

        dc.SelectObject(wx.NullBitmap)
        return ytext1 + 2*ytext2 + 40
    

    def GetSubItemWidth(self):

        return 250
    

    def DrawHorizontalPipe(self, dc, x, y, w, colour):
        """Draws a horizontal 3D-looking pipe."""
        
        for r in range(PIPE_HEIGHT):
            red = int(colour.Red() * math.sin((math.pi/PIPE_HEIGHT)*r))
            green = int(colour.Green() * math.sin((math.pi/PIPE_HEIGHT)*r))
            blue = int(colour.Blue() * math.sin((math.pi/PIPE_HEIGHT)*r))
            dc.SetPen(wx.Pen(wx.Colour(red, green, blue)))
            dc.DrawLine(x, y+r, x+w, y+r)


    def DrawProgressBar(self, dc, x, y, w, h, percent):
        """
        Draws a progress bar in the (x,y,w,h) box that represents a progress of 
        'percent'. The progress bar is only horizontal and it's height is constant 
        (PIPE_HEIGHT). The 'h' parameter is used to vertically center the progress 
        bar in the allotted space.
        
        The drawing is speed-optimized. Two bitmaps are created the first time this
        function runs - one for the done (green) part of the progress bar and one for
        the remaining (white) part. During normal operation the function just cuts
        the necessary part of the two bitmaps and draws them.
        """
                
        #Create two pipes
        if self.DONE_BITMAP is None:
            self.DONE_BITMAP = wx.EmptyBitmap(PIPE_WIDTH, PIPE_HEIGHT)
            mdc = wx.MemoryDC()
            mdc.SelectObject(self.DONE_BITMAP)
            self.DrawHorizontalPipe(mdc, 0, 0, PIPE_WIDTH, wx.GREEN)
            mdc.SelectObject(wx.NullBitmap)

            self.REMAINING_BITMAP = wx.EmptyBitmap(PIPE_WIDTH, PIPE_HEIGHT)
            mdc = wx.MemoryDC()
            mdc.SelectObject(self.REMAINING_BITMAP)
            self.DrawHorizontalPipe(mdc, 0, 0, PIPE_WIDTH, wx.RED)
            self.DrawHorizontalPipe(mdc, 1, 0, PIPE_WIDTH-1, wx.WHITE)
            mdc.SelectObject(wx.NullBitmap)

        # Center the progress bar vertically in the box supplied
        y = y + (h - PIPE_HEIGHT)/2 

        if percent == 0:
            middle = 0
        else:
            middle = (w * percent)/100

        if w < 1:
            return
        
        if middle == 0: # not started
            bitmap = self.REMAINING_BITMAP.GetSubBitmap((1, 0, w, PIPE_HEIGHT))
            dc.DrawBitmap(bitmap, x, y, False)
        elif middle == w: # completed
            bitmap = self.DONE_BITMAP.GetSubBitmap((0, 0, w, PIPE_HEIGHT))
            dc.DrawBitmap(bitmap, x, y, False)
        else: # in progress
            doneBitmap = self.DONE_BITMAP.GetSubBitmap((0, 0, middle, PIPE_HEIGHT))
            dc.DrawBitmap(doneBitmap, x, y, False)
            remainingBitmap = self.REMAINING_BITMAP.GetSubBitmap((0, 0, w - middle, PIPE_HEIGHT))
            dc.DrawBitmap(remainingBitmap, x + middle, y, False)


            
class taskList(ULC.UltimateListCtrl,listmix.ListCtrlAutoWidthMixin):
    def __init__(self,parent,id):
        ULC.UltimateListCtrl.__init__(self,parent, id, pos=wx.DefaultPosition, size=wx.DefaultSize, style=0, agwStyle=wx.LC_REPORT| wx.BORDER_SUNKEN
                                         #| wx.BORDER_NONE
                                         #| wx.LC_SORT_ASCENDING
                                         #| wx.LC_NO_HEADER
                                         #| wx.LC_VRULES
                                         | wx.LC_HRULES
                                         #| wx.LC_SINGLE_SEL
                                         | ULC.ULC_HAS_VARIABLE_ROW_HEIGHT)
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        self.PopulateList()
        
        #self.Bind(wx.EVT_TIMER, self.OnTimer)
        #self.t1 = wx.Timer(self)
        #self.t1.Start(1000)
        #def OnTimer(self,evt):
        #self.RefreshItems(0,100)

    def PopulateList(self):

        self.Freeze()

        info = ULC.UltimateListItem()
        info._mask = wx.LIST_MASK_TEXT | wx.LIST_MASK_FORMAT
        info._format = 0
        info._text = "Task List"
        
        self.InsertColumnInfo(0, info)

        self.SetColumnWidth(0, 150)
        #self.SetColumnWidth(1, 300)
        self.Thaw()
        self.Update()

    def pushTask(self,cfgName):
        # check exist
        ki = -1
        while True:
            ki = self.GetNextItem(ki,
                                     wx.LIST_NEXT_ALL)
            if ki == -1:
                break
            dat = self.GetItemData(ki)
            if dat.title == cfgName:
                dat.wthread.stop()
                while dat.wthread.isAlive(): pass
                dat.restart()
                return

        #if not exist, add new item
        index = self.InsertStringItem(sys.maxint, "")
        klass = taskRenderer(self,cfgName)
        self.SetItemCustomRenderer(index, 0, klass)
        self.SetItemData(index,klass)
        klass.index = index
        klass.Updater = self.RefreshItem
        klass.restart()
        #index = self.Append("%s : %s : 0%%" % (cfgName,taskIm.__getFormatedElapsedTime())]
    def get_sel_cfgname(self):
        ki = -1
        bDelAnything = False
        while True:
            ki = self.GetNextItem(ki,
                                     wx.LIST_NEXT_ALL,
                                     wx.LIST_STATE_SELECTED)
            if ki == -1:
                break
            dat = self.GetItemData(ki)
            return dat.title
    def DeleteSelTasks(self):
        ki = -1
        bDelAnything = False
        while True:
            ki = self.GetNextItem(ki,
                                     wx.LIST_NEXT_ALL,
                                     wx.LIST_STATE_SELECTED)
            if ki == -1:
                break
            bDelAnything = True
            #stop thread
            dat = self.GetItemData(ki)
            dat.wthread.stop()
            #rm
            self.DeleteItem( ki )
            
        #re-arange after all sel-ed be del-ed
        if bDelAnything:
            ki = -1
            while True:
                ki = self.GetNextItem(ki,
                                     wx.LIST_NEXT_ALL)
                if ki ==-1:
                    break
                #reset the index
                dat = self.GetItemData(ki)
                dat.index = ki
            
            
            #print "%d is sel" % ki
class workThread(threading.Thread):
    def __init__(self,taskItem,cfgfileName):
        threading.Thread.__init__(self)
        self.sbp = None
        self.taskItem=taskItem
        self.__run = True
        self.cfgfileName =cfgfileName
    def stop(self):
        self.__run = False
        try:
            self.sbp.kill()
        except Exception, e:
            # raise e
            print 'killing subprocess occured an error.'
    
    def _func(self):
        #evt=postEvent(evtTypePost)
        #evt2=postEvent(evtTypePost,False)
        self.taskItem.UpdateDoneFlag(False)
        cmd ='python mods/main.py %s' % self.cfgfileName
        print '  - run: ' + cmd
        self.sbp = subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True)
        pipeFile = self.sbp.stdout


        pipe.reset_cfg(self.cfgfileName[4:])

        while self.__run:
            readData=pipeFile.readline()
            if readData=='':
                print "%s : work thread got an error, or terminated!" % self.cfgfileName
                self.stop()
                break
            #判断是否是进度信息，截取
            if readData[:2]=='->':
                #evt.pos=
                #wx.PostEvent(self.frame,evt)
                self.taskItem.progressValue=int(readData[2:]) 
                self.taskItem.UpdateTime()
                #dangerous here,might not thread safe!!
                #mark
                self.taskItem.Updater(self.taskItem.index)
                continue
            elif readData[:2]== 'E:':
                print readData
            elif readData[:2]== '<-':
                print "%s : work finished!" % self.cfgfileName
                self.stop()
                break
            elif readData[:2]== 'M:':
                pipe.populate(self.cfgfileName[4:], readData[2:])
            elif readData[:2]== 'D:':
                try:
                    pipe.pushdata(self.cfgfileName[4:], readData[2:])
                except Exception, e:
                    self.stop()
                    break
            else:
                print readData

            #evt2.strdata=readData
            #wx.PostEvent(self.frame,evt2)#self.frame.GetEventHandler().ProcessEvent(evt2)
        self.taskItem.UpdateDoneFlag()
        self.taskItem.Updater(self.taskItem.index)
        pipeFile.close()
        print '%s : work thread Done!!' % self.cfgfileName
    def run(self):
        try:
            self._func()
        except Exception, e:
            print "thread error!"
