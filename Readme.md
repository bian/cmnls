密集介质中的薛定鄂方程模拟
源代码分发协议：GPL协议

最新的源代码使用git工具签出，[这里](http://pan.baidu.com/s/1kTFSHef)有最近20140525的一份源代码可以直接下载

        
## 如何运行程序，不要错过以下每一个步骤
1. 安装python2.7
2. 安装matplotlib， numpy，scipy，wxpython对应python2.7的版本


**最新版本可以自己谷歌搜索官网下载，或者直接在[这里](http://pan.baidu.com/s/1i3Ka3IX)下载我已经打包好的**


3. 配置环境变量，将python.exe所在目录加入PATH环境变量


首先，进入系统设置，右键我的电脑(winXp/win7)或者右键左下角的开始菜单处（win8）


![s.png](https://bitbucket.org/repo/64z9aL/images/3990751802-s.png)


然后进入环境变量设置页面


![QQ截图20140525163058.png](https://bitbucket.org/repo/64z9aL/images/2651719095-QQ%E6%88%AA%E5%9B%BE20140525163058.png)


在Path变量的设置里面最后加入计算机内安装的python路径的根目录，注意与前面目录之间用英文的分号隔开，最后不需要加分号


![QQ截图20140525163356.png](https://bitbucket.org/repo/64z9aL/images/2136596438-QQ%E6%88%AA%E5%9B%BE20140525163356.png)


4. 运行
使用cmd，cd 到项目的根目录，运行`python App.py`即可
详解：打开系统的命令提示符，cd命令表示change directory，改变目录，下载源代码，解压至E盘根目录，比如E:\wk_\py\cmnls，可以按照如下几个命令输入回车运行


![QQ截图20140525164825.png](https://bitbucket.org/repo/64z9aL/images/1435105209-QQ%E6%88%AA%E5%9B%BE20140525164825.png)


## 如何使用这个程序 ##
* 以模拟紧聚焦的飞秒脉冲在石英玻璃内部传输为例
* 改变所有参数至如图所示


![QQ截图20140525165211.png](https://bitbucket.org/repo/64z9aL/images/4076303896-QQ%E6%88%AA%E5%9B%BE20140525165211.png)


* 点击Calculate开始运行，右侧会出现当前任务的进度条，如果想取消运行可以选中任务，点Remove
* 运行结束后，查看结果选择画图功能列表的第二个工具，执行excute即可看到结果


![QQ截图20140525165610.png](https://bitbucket.org/repo/64z9aL/images/3529193854-QQ%E6%88%AA%E5%9B%BE20140525165610.png)


![QQ截图20140525165858.png](https://bitbucket.org/repo/64z9aL/images/14816457-QQ%E6%88%AA%E5%9B%BE20140525165858.png)

* 程序根目录介绍


cfg:


    参数配置文件目录


cfg/data:


    对应参数配置的运算结果目录


* 其他小工具：load cfg表示加载之前的运算参数；diff cfg表示对比当前的参数与任何选择的参数配置，并且返回差异到命令行