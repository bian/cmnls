#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib.gridspec as gridspec

def adjust_spines(ax,spines):
    # turn off ticks where there is no spine
    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])

def getaxis(dl):
    pass

def main(fig):
    grid = AxesGrid(fig, 111, # similar to subplot(132)
                    nrows_ncols = (4, 1),
                    axes_pad = 0.0,
                    share_all=True,
                    label_mode = "L",
                    cbar_location = "right",
                    cbar_mode="single",
                    cbar_size="1%",
                    cbar_pad="1%",
                    )
    #plt.colorbar(im, cax = grid.cbar_axes[0])
    ax1 = grid[0]
    ax2= grid[1]
    ax3= grid[2]
    ax4= grid[3]
    for cax in grid.cbar_axes:
        cax.toggle_label(True)

    return ax1, ax2, ax3, ax4, grid
if __name__ == '__main__':
    main()
