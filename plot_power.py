#!/usr/bin/env python
# -*- coding: utf-8 -*-


#import wx
import numpy as np
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker
from numpy import ma

from mpl_toolkits.mplot3d import axes3d
import sys

arglen=len(sys.argv)
if arglen!=2:
    print 'no specific dest file'
    exit()

fig = plt.figure()


db=np.load(sys.argv[1])
plotr=db['plotr']
distances=db['distances']
plott=db['plott']
intensity=db['intensity']
eleDensity=db['eleDensity']
cmaper = cm.Reds
lenr = len(plotr)
lent = len(plott)
lend = len(distances)

dr = plotr[2]-plotr[1]
dt = plott[2]-plott[1]
assert dt > 0 and dr>0
#print plotr[0], plotr[1], plotr[2]
assert plotr[0] < 1e-15
##show power-z

va = np.zeros(plotr.shape)
for ki in range(lenr):
    va[ki] = np.pi * ((plotr[ki] + dr )**2 - plotr[ki]**2)

ca = np.ones([1, lent])
vc = np.dot(va,ca)
vb = np.zeros(intensity.shape)
power = np.zeros([lent, lend])
for ki in range(lend):
    #print(intensity.shape, plotr.shape, plott.shape, intensity[:,:,ki].shape,vc.shape)
    vb[:,:,ki] = intensity[:,:,ki] * vc
power[:, :] = np.trapz(vb, dx=dr, axis=0)

powerPeak = power.max(0)

ax = fig.add_subplot(2, 1,1)
ax.plot(distances, powerPeak)
#energy
ax = fig.add_subplot(2,1,2)
Energy=np.trapz(power,dx=dt,axis=0)
ax.plot(distances, Energy)




plt.show()

if __name__ == '__main__':
    pass
