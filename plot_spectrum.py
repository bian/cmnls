#!/usr/bin/env python
# -*- coding: utf-8 -*-


#import wx
import numpy as np
from numpy import ma
from numpy import *
import scipy as sp
from scipy.fftpack import fft, ifft, fftshift
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable

from mpl_toolkits.mplot3d import axes3d
import sys

arglen=len(sys.argv)

cmaper = cm.Reds
div =1

def get_peak_intensity(inten):
    Zm=inten.max(1)
    #Zmax = Zm.max()
    Z=np.vstack([Zm[::-1,:],Zm[1:]])
    return Z

def add_colorbar_axis(axis, direction = "right"):
    divider = make_axes_locatable(axis)
    axc = divider.append_axes(direction, size="3%", pad=0.05)
    return axc

def plot_3d_intensity(axis, X, Y, intensity):
    Z= get_peak_intensity(intensity)
    surf=axis.plot_surface(X, Y, Z, rstride=div, cstride=div, cmap=cm.summer, linewidth=0)
#~ cmap=cm.jet,,linewidth=0, antialiased=True
    axis.set_xlabel('Distance(um)')
    axis.set_ylabel('Radius(um)')
    axis.set_zlabel('Intensity')
    #ax.set_title(r'Peak intensity($W/m^2$)')
    return surf

def plot_2d_intensity(axis, X, Y, inten, bGridmode = False):
    #Zmasked=ma.masked_where(Z < Zmax/10 ,Z)
    Z= get_peak_intensity(inten)

    Z=Z/1e4
    surf=axis.contourf(X,Y,Z,origin='lower',cmap=cmaper)
    #surf.cmap.set_under('yellow')
    #surf.cmap.set_over('white')
    if not bGridmode:
        plt.colorbar(surf, shrink=.5)
    #~ orientation='horizontal',
        axis.set_xlabel('distance(um)')
        axis.set_ylabel('radius(um)')
        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        axis.text(0.05, 0.95, 'Peak intensity($W/cm^2$)', transform=axis.transAxes, fontsize=14,
            verticalalignment='top', bbox=props)
    return surf
def plot_fluence(axis, X, Y, plott, intens, bGridmode = False):
    Energy=np.trapz(intens,dx=plott[2]-plott[1],axis=1)
    Z=np.vstack([Energy[::-1,:],Energy[1:]])
    Z=Z/1e4#m -> cm
        #Zmasked = ma.masked_where(Z < Z.max()/100, Z)

    surf=axis.contourf(X,Y,Z,origin='lower',cmap=cmaper)
    if not bGridmode:
        plt.colorbar(surf, shrink=0.5)
        #~ orientation='horizontal',
        axis.set_xlabel('distance(um)')
        axis.set_ylabel('radius(um)')
        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        axis.text(0.05, 0.95, 'Fluence ($J/cm^2$)', transform=axis.transAxes, fontsize=14,
            verticalalignment='top', bbox=props)

    return surf
def plot_density(axis, X, Y, plott, edens, bInGrid = False):
    idx=0    #找到50fs处的索引
    for ki in plott:
        if ki>50e-15 :
            break
        idx+=1
    ed50fs=edens[:,idx,:]
    Z=np.vstack([ed50fs[::-1,:],ed50fs[1:]])
    Z=Z/1e6

    #Zmasked = ma.masked_where(Z < Z.max()/100, Z)
    surf=axis.contourf(X,Y,Z,cmap=cmaper)
    if not bInGrid:
        axc=add_colorbar_axis(axis)
        plt.colorbar(surf, cax=axc)
        axis.set_xlabel('distance(um)')
        axis.set_ylabel('radius(um)')
        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        axis.text(0.05, 0.95, 'Electron density($1/cm^3$)', transform=axis.transAxes, fontsize=14, verticalalignment='top', bbox=props)
    return surf
def parse_file(file):
    db=np.load(file)
    plotr=db['plotr']*1e6
    distances=db['distances']*1e6
    plott=db['plott']
    intensity=db['intensity']
    eleDensity=db['eleDensity']
    real_fields = db["fields"]
    return plotr, plott, distances, intensity, eleDensity, real_fields



def normaliser(d):
    return d/max(d)
def plot_spectrum_at_z(shape_r, shape_t, f, z):
    f1 = np.zeros(shape_t)
    f1[:,0] = f[0,:,z].squeeze()
    return normaliser(1/abs(fftshift(fft(f1))))
def main():
    fig = plt.figure()
    # plotr, plott, distances, intensity, eleDensity,real_fields = parse_file(sys.argv[1])
    plotr, plott, distances, intensity, eleDensity,real_fields = parse_file('.//cfg//data//cfg_d75_Nr80_U10.npz')

    f0 = plot_spectrum_at_z(plotr.shape, plott.shape,real_fields,0)
    f1 = plot_spectrum_at_z(plotr.shape, plott.shape,real_fields,distances.shape[0]/4)
    f2 = plot_spectrum_at_z(plotr.shape, plott.shape,real_fields,distances.shape[0]/2)
    f3 = plot_spectrum_at_z(plotr.shape, plott.shape,real_fields,distances.shape[0]-1)
    ax = fig.add_subplot(1, 2, 1)
    plt.plot(f0)
    plt.title('initial amplitude')
    ax = fig.add_subplot(1, 2, 2)
    plt.plot(f0,'r--',f1,'g--s',f2,'b--^',f3,'black')
    plt.title('z=0,max/4,max/2,max')

    # ax = fig.add_subplot(2, 2, 4)

    plt.show()


if __name__ == '__main__':
    # if arglen<2:
    #     print 'no specific dest file'
    #     exit()
    # elif arglen == 3 and sys.argv[2] == '1':
    #     div = 8
    main()
