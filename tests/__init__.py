import unittest
from tests.derivetest import funcTestCase

def all_tests():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(funcTestCase))
    return suite
