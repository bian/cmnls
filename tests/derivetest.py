#coding=utf-8
import unittest, os

import numpy as np
import scipy as sp
from scipy.misc import derivative

import derive

def d_standard_li(x0, d = 1):
    return derivative(lambda x: x*2, x0, dx=d)
def d_standard_sq(x0, d = 1):
    return derivative(lambda x: x**2, x0, dx=d)
def d_standard_gs(x0, d = 1, n = 1):
    return derivative(lambda x: np.exp(-(x/0.12)**2), x0, dx=d, n=n) 

def eq(x1, x2, diff=1e-12):
    '''
    check if nearly equal
    x1,x2: numpy array
    '''
    if np.all(abs(x1.real - x2.real) < diff): return True
    print '-'*50
    print '\n ->', x1, 'not equal', x2
    print ' -> their diff',abs(x1.real - x2.real)
    print '-'*50
    return False

class funcTestCase(unittest.TestCase):
    def setUp(self):
        #guassion shape
        x=np.arange(-20,13)[:,np.newaxis]
        x=x/33.
        self.x=x=x.T
        self.dx=x[0,1]-x[0,0]#0.0303..
        self.xT=x[0,-1]-x[0,0]
        self.li = x*2
        self.sq = x**2
        self.gs = np.exp(-(x/0.12)**2)

    def tearDown(self):
        pass
    def test_deriver(self):
        d = derive.deriver(self.li, dx=self.dx, row=True)
        self.assertTrue(eq(d, d_standard_li(self.x, d=self.dx)))
        d = derive.deriver(self.sq, dx=self.dx)
        self.assertTrue(eq(d, d_standard_sq(self.x, d=self.dx)))
        #高斯形状,误差大
        d = derive.deriver(self.gs, dx=self.dx)
        self.assertTrue(eq(d, d_standard_gs(self.x, d=self.dx), diff =1))
        d = derive.deriver(self.li.T, dx=self.dx, row=False)
        self.assertTrue(eq(d, d_standard_li(self.x.T, d=self.dx)))
        d = derive.deriver(self.sq.T, dx=self.dx, row=False)
        self.assertTrue(eq(d, d_standard_sq(self.x.T, d=self.dx)))
        d = derive.deriver(self.gs.T, dx=self.dx, row=False)
        self.assertTrue(eq(d, d_standard_gs(self.x.T, d=self.dx), diff =1))
    def test_derive_fft(self):
        d = derive.derive_fft(self.gs, self.xT, row=True)
        dgs = -2*self.x*self.gs/0.12**2
        self.assertTrue(eq(d, dgs, diff =1))
        self.assertTrue(eq(d, d_standard_gs(self.x, d=self.dx), diff =1))
        self.assertTrue(eq(dgs, d_standard_gs(self.x, d=self.dx), diff =1))
        
        d = derive.derive2_fft(self.gs, self.xT, row=True)
        self.assertTrue(eq(d, d_standard_gs(self.x, d=self.dx, n=2), diff =15))

        d = derive.derive_fft(self.gs.T, self.xT, row=False)
        self.assertTrue(eq(d, d_standard_gs(self.x.T, d=self.dx), diff =1))
        d = derive.derive2_fft(self.gs.T, self.xT, row=False)
        self.assertTrue(eq(d, d_standard_gs(self.x.T, d=self.dx, n=2), diff =15))
    def test_derive_symmetric_fft_column(self):
        df=derive.derive_symmetric_fft_column(self.gs.T,self.x[0,-1]-self.x[0,0])
        df1=df.T