﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-


#import wx
import numpy as np
from numpy import ma
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.mplot3d import axes3d
import sys

from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
font = FontProperties(fname=r"c:\windows\fonts\simhei.ttf", size=14) 

arglen=len(sys.argv)

# cmaper = cm.Reds
# cmaper = cm.binary_r
cmaper = cm.gist_heat
# cmaper = cm.YlOrRd
# cmaper = cm.Greys
div =1

def add_colorbar_axis(axis, direction = "right"):
    divider = make_axes_locatable(axis)
    axc = divider.append_axes(direction, size="3%", pad=0.05)
    return axc

def plot_3d_intensity(axis, X, Y, peakIntensity):
    Z= peakIntensity/1e4
    surf=axis.plot_surface(X, Y, Z, rstride=div, cstride=div, cmap=cm.summer, linewidth=0)
#~ cmap=cm.jet,,linewidth=0, antialiased=True
    axis.set_xlabel('z(um)')
    axis.set_ylabel('r(um)')
    axis.set_zlabel('Intensity')
    #ax.set_title(r'Peak intensity($W/m^2$)')

def plot_deposited_energy_density(axis, X, Y, depositedEnergyDensity, bGridmode = False):
    #Zmasked=ma.masked_where(Z < Zmax/10 ,Z)
    Z=depositedEnergyDensity/1e6
    surf=axis.contourf(X,Y,Z,origin='lower',cmap=cmaper)
    axis.yaxis.set_ticks([-6,-3,0,3,6])
    # surf.set_clim(vmin=50)
    #surf.cmap.set_under('yellow')
    #surf.cmap.set_over('white'), ticks=[50, 200, 400, 600, 800, 1000, 1200]
    if not bGridmode:
        plt.colorbar(surf,pad=0.005,shrink=0.8)
        plt.setp(axis.get_xticklabels(), visible=False)
    #~ orientation='horizontal',
        axis.set_xlabel('z(um)')
        axis.set_ylabel('r(um)')
        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        axis.text(0.05, 0.95, 'Deposited Energy Density ($J/cm^3$)', transform=axis.transAxes, fontsize=14,
            verticalalignment='top', bbox=props,color="w")

def plot_2d_intensity(axis, X, Y, peakIntensity, bGridmode = False):
    #Zmasked=ma.masked_where(Z < Zmax/10 ,Z)
    Z= peakIntensity/1e4
    surf=axis.contourf(X,Y,Z,origin='lower',cmap=cmaper)
    axis.yaxis.set_ticks([-6,-3,0,3,6])
    surf.set_clim(vmin=1e12)
    #surf.cmap.set_under('yellow')
    #surf.cmap.set_over('white'), ticks=[2e12,5e12,8e12,1e13,2e13,3e13]ticks=[8e11,2e13,4e13,6e13,9e13],
    if not bGridmode:
        cbb = plt.colorbar(surf, pad=0.005,  shrink=0.8,format="%.0e")
        cbb.locator = MaxNLocator( nbins = 4)
        cbb.update_ticks()
        plt.setp(axis.get_xticklabels(), visible=False)
    #~ orientation='horizontal',
        # axis.set_xlabel('Distance(um)')
        # axis.set_ylabel('r(um)')
        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        axis.text(0.05, 0.95, u'峰值场强($W/cm^2$)', transform=axis.transAxes, fontsize=14,
            verticalalignment='top', bbox=props,color="w", fontproperties=font)

def plot_fluence(axis, X, Y, plott, fluence, bGridmode = False):
    Z=fluence/1e4#m -> cm
        #Zmasked = ma.masked_where(Z < Z.max()/100, Z), ticks=[0.1, 0.5,1.0,1.5,2.1]

    surf=axis.contourf(X,Y,Z,origin='lower',cmap=cmaper)
    axis.yaxis.set_ticks([-6,-3,0,3,6])
    surf.set_clim(vmin=0.2)
    if not bGridmode:
        cbb = plt.colorbar(surf, pad=0.005, shrink=.8)
        cbb.locator = MaxNLocator( nbins = 4)
        cbb.update_ticks()
        #~ orientation='horizontal',
        axis.set_xlabel(r'$z/\mu m$')
        # axis.set_ylabel('radius(um)')
        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        axis.text(0.05, 0.95, u'能流($J/cm^2$)', transform=axis.transAxes, fontsize=14,
            verticalalignment='top', bbox=props,color="w", fontproperties=font)

def plot_density(axis, X, Y, plott, edens, bInGrid = False):
    
    Z = edens/1e6
    # Zmasked = ma.masked_where(Z < Z.max()/1e6, Z)ticks=[1e15, 1e17, 1e18, 1e19, 3e20],
    surf=axis.contourf(X,Y,Z,cmap=cmaper)
    axis.yaxis.set_ticks([-6,-3,0,3,6])
    # surf.set_clim(vmin=1e15)
    if not bInGrid:
        # axc=add_colorbar_axis(axis)
        cbb = plt.colorbar(surf,pad=0.005, shrink=0.8,format="%.0e")# cax=axc)
        cbb.locator = MaxNLocator( nbins = 4)
        cbb.update_ticks()
        plt.setp(axis.get_xticklabels(), visible=False)
        # axis.set_xlabel('distance(um)')
        axis.set_ylabel(r'$r/\mu m$')
        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        axis.text(0.05, 0.95, u'自由电子态密度($1/cm^3$)', 
            transform=axis.transAxes, fontsize=14, verticalalignment='top', bbox=props,color="w", fontproperties=font)
def parse_file(file):
    db=np.load(file)
    plotr=db['plotr']*1e6
    distances=db['distances']*1e6
    plott=db['plott']
    intensity=db['intensity']
    eleDensity=db['eleDensity']
    return plotr, plott, distances, intensity, eleDensity
def parse_variable_from_file(file):
    db=np.load(file)
    plotr=db['plotr']
    
    distances=db['distances']
    plott=db['plott']
    intensity=db['intensity']
    depositedIdz = db['depositedIdz']
    eleDensity=db['eleDensity']
    Fluence=np.trapz(intensity,dx=plott[2]-plott[1],axis=1)
    depositedEnergyDensity = np.trapz(depositedIdz,dx=plott[2]-plott[1],axis=1)

    idx=0    #找到50fs处的索引
    for ki in plott:
        if ki>50e-15 :
            break
        idx+=1
    eleDensity=eleDensity[:,idx,:]

    startindex = 0
    if plotr[0] != 0 : startindex = 1

    plotr=np.vstack([-plotr[::-1,:],plotr[startindex:]])
    single_peak_I = intensity.max(1)
    assert len(single_peak_I.shape) is 2
    peakIntensity=np.vstack([single_peak_I[::-1,:],single_peak_I[startindex:]])
    # print peakIntensity.shape
    eleDensity=np.vstack([eleDensity[::-1,:],eleDensity[startindex:]])
    Fluence=np.vstack([Fluence[::-1,:],Fluence[startindex:]])
    depositedEnergyDensity = np.vstack([depositedEnergyDensity[::-1,:],depositedEnergyDensity[startindex:]])

    return plotr, plott, distances, peakIntensity, eleDensity, Fluence, depositedEnergyDensity

def main():
    fig = plt.figure()
    plotr, plott, distances, peakIntensity, eleDensity,Fluence,depositedEnergyDensity= parse_variable_from_file(sys.argv[1])
    X, Y = np.meshgrid(distances*1e6,plotr*1e6)
    plt.subplots_adjust(hspace=0.001)
    # ax = plt.subplot(4, 1, 1)
    # plot_deposited_energy_density(ax, X, Y, depositedEnergyDensity)
    # plot_3d_intensity(ax, X, Y, peakIntensity)
    # ax.set_ylim(-80,80)
    # ax.set_ylim(-8,8)
    ax = plt.subplot(4, 1, 1)
    plot_2d_intensity(ax, X, Y, peakIntensity)
    ax.set_ylim(-8,8)
    ax = plt.subplot(4, 1, 2)
    plot_density(ax, X, Y, plott, eleDensity)
    ax.set_ylim(-8,8)
    ax = plt.subplot(4, 1, 3)
    plot_fluence(ax, X, Y, plott, Fluence)
    ax.set_ylim(-8,8)
    ax = plt.subplot(4, 1, 4)
    plot_deposited_energy_density(ax, X, Y, depositedEnergyDensity)
    ax.set_ylim(-8,8)
    plt.show()


if __name__ == '__main__':
    if arglen<2:
        print 'no specific dest file'
        exit()
    elif arglen == 3 and sys.argv[2] == '1':
        div = 8
    main()
