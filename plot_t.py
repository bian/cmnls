#!/usr/bin/env python
# -*- coding: utf-8 -*-


import wx
import numpy as np
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker

from mpl_toolkits.mplot3d import axes3d
import sys

arglen=len(sys.argv)
if arglen!=2:
    print 'no specific dest file'
    exit()

fig = plt.figure()


db=np.load(sys.argv[1])
plotr=db['plotr']
distances=db['distances']
plott=db['plott']
intensity=db['intensity']
eleDensity=db['eleDensity']
#~ X=plotr
#~ Y=plott
#print plotr
hasrad0=False
if plotr[0]==0:
    hasrad0=True
## if hasrad0:
##     plotr=np.vstack([-plotr[::-1,:],plotr[1:]])
## else:
##     plotr=np.vstack([-plotr[::-1,:],plotr])
#
#exit()
X, Y = np.meshgrid(distances,plott)

ax = fig.add_subplot(2, 2, 1,projection='3d')
#r=0,
Z=intensity[0,:,:]
#Z=intensity.max(0)
## Zm=intensity.max(0)
## if hasrad0:
##     Z=np.vstack([Zm[::-1,:],Zm[1:]])
## else:
##     Z=np.vstack([Zm[::-1,:],Zm])

surf=ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2)
#~ cmap=cm.jet,,linewidth=0, antialiased=True
ax.set_xlabel('distance/m')
ax.set_ylabel('time/s')
ax.set_zlabel('intensity(max) $W/m^2$')
ax.set_title(r'Time-distance-I ($W/m^2$) along z(r=0),3D')
#~ fig.colorbar(surf, shrink=0.5, aspect=5)
formatter = ticker.FormatStrFormatter('%2.1e')
ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_major_formatter(formatter)


ax = fig.add_subplot(2, 2, 2)
surf=ax.contourf(X,Y,Z,origin='lower')
#surf.cmap.set_under('yellow')
#surf.cmap.set_over('white')

plt.colorbar(surf, shrink=.5)
#~ orientation='horizontal', 
ax.set_xlabel('distance/m')
ax.set_ylabel('time/s')
ax.set_title('Maxima intensity ($W/m^2$)(r=0) along z')

formatter = ticker.FormatStrFormatter('%2.1e')
ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_major_formatter(formatter)


Z=intensity[len(plotr)/4,:,:]
ax = fig.add_subplot(2, 2, 3)
surf=ax.contourf(X,Y,Z,origin='lower')
#surf.cmap.set_under('yellow')
#surf.cmap.set_over('white')

plt.colorbar(surf, shrink=.5)
#~ orientation='horizontal', 
ax.set_xlabel('distance/m')
ax.set_ylabel('time/s')
ax.set_title('Maxima intensity ($W/m^2$)(r=Rmax/4) along z')

formatter = ticker.FormatStrFormatter('%2.1e')
ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_major_formatter(formatter)


Z=intensity[len(plotr)/2,:,:]
ax = fig.add_subplot(2, 2, 4)
surf=ax.contourf(X,Y,Z,origin='lower')
#surf.cmap.set_under('yellow')
#surf.cmap.set_over('white')

plt.colorbar(surf, shrink=.5)
#~ orientation='horizontal', 
ax.set_xlabel('distance/m')
ax.set_ylabel('time/s')
ax.set_title('Maxima intensity ($W/m^2$)(r=Rmax/2) along z')

formatter = ticker.FormatStrFormatter('%2.1e')
ax.yaxis.set_major_formatter(formatter)
ax.xaxis.set_major_formatter(formatter)

plt.show()


