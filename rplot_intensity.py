#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import AxesGrid
from numpy import ma

from mpl_toolkits.mplot3d import axes3d
import sys

import plot_cl as pcl
import view4x1 as v41

def parse_from_file(file):
    plotr, plott, distances, intensity, eleDensity = pcl.parse_file(file)
    plotr=np.vstack([-plotr[::-1,:],plotr[1:]])

    X, Y = np.meshgrid(distances,plotr)
    return X, Y, intensity


def main():

    arglen=len(sys.argv)
    if arglen<2:
        print 'no specific dest file'
        exit()

    fig = plt.figure()

    assert arglen-1 == 4

    ax1,ax2,ax3,ax4, grid=v41.main(fig)

    for i in range(4):
        X, Y, intens = parse_from_file(sys.argv[i+1])
        surf = pcl.plot_2d_intensity(eval("ax"+str(i+1)), X, Y, intens, True)
        #plt.colorbar(surf, cax = grid.cbar_axes[0])
    ax4.cax.colorbar(surf)
    fig.text(0.06, 0.5, 'r(um)', ha='center', va='center', rotation='vertical')
    ax4.set_xlabel("z(um)")
    plt.draw()
    plt.show()


if __name__ == '__main__':
    main()
