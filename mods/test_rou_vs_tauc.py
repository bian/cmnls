#!/bin/python
#coding=utf-8

import numpy as np
import numpy.ma as ma
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.io as sio
import scipy.special as sps
import sympy


def main():
    import main
    dat=main.dataPools()

    dampterm = np.array(np.linspace(1, 33,40))
    # print dampterm
    rou_array=dat.k * dampterm/(dat.critical_electron_density * (1+dampterm**2 )  )
    # print rou_array
    tauc_array = np.array(np.linspace(1.2e-15, 2.7e-14,40))
    dampterm_array = dat.w0*tauc_array
    rou_array2=dat.k * dampterm_array/(dat.critical_electron_density * (1+dampterm_array**2 )  )


    # plt.subplot(121)
    # plt.plot(dampterm,rou_array)
    # plt.ylabel('$rou (m^{-2})$')
    # plt.xlabel('$dampterm$')
    # plt.subplot(122)
    plt.semilogy(tauc_array*1e15,rou_array2/1e4, 'k')
    plt.ylabel(r'$\sigma /cm^{2}$')
    plt.xlabel(r'$\tau_c /fs$')
    plt.show()

if __name__ == '__main__':
    main()