#!/bin/python
#coding=utf-8

import numpy as np
import numpy.ma as ma
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.io as sio
import scipy.special as sps
#from sympy import *
#import sympy

from derive import *

from mpi import _wpiDivI


def get_ed(data,I, EnergyGap, b_ava=True):
    # assertion for I, in case of overflow
    # another reason is that for out of range of weak intensity
    #  should use keldysh formulation instead
    # assert I.max() < 1e+19
    # calculate wpi rate

    taur=150e-15#s
    wpi_div_I = _wpiDivI(I,data)
    wpi = wpi_div_I*I

    szE=np.shape(I)
    D=np.zeros(szE)
    for ki in xrange(1,szE[1]):
        D_slice = D[:,ki-1]
        rest_poss = 1.0 - D_slice/data.atomDensity
        total_rate = wpi[:,ki-1]*rest_poss - D_slice/taur
        # total_rate = wpi[:,ki-1] - D_slice/taur
        if b_ava: 
            # ava_rate = data.rouDivUi*(D_slice*I[:,ki-1])/((1+data.reduced_mass_rate) * data.n0**2)
            ava_rate = (I[:,ki-1]/EnergyGap[:,ki-1])*(data.rou*D_slice)/(1+data.reduced_mass_rate)
            # ava_rate = data.rouDivUi*(D_slice*I[:,ki-1])
            total_rate += ava_rate*rest_poss
            # total_rate += ava_rate
        ednow=D_slice+total_rate*data.dt
        D[:,ki]=ma.masked_greater(ednow, data.atomDensity).filled(data.atomDensity)
        
    return D, wpi_div_I


if __name__=='__main__':
    import main
    dat=main.dataPools()
    pulse=main.getGaussPulse(dat)
    pulse *= 3.8
    trueI=abs(pulse)**2
    dat.trueTimes *= 1e15

    # I = Es

    fig = plt.figure()
    plt.subplots_adjust(hspace=0.001, wspace = 0.001)
    tauc_array = (0.4e-15, 1.27e-15, 4e-15, 10e-15)
    axis1 = None
    ax1,ax3 = None,None
    for x in xrange(1,5):
        if x==1:
            cax=ax1 = plt.subplot(221)
            plt.setp(ax1.get_xticklabels(), visible=False)
            plt.tick_params(
                axis='y',          
                which='both',     
                right='off',     
                labelright='off')
            plt.tick_params(
                axis='y',          
                which='minor',     
                left='off')
            plt.tick_params(
                axis='x',          
                which='both',     
                bottom='off',     
                labelbottom='off')
        elif x==2:
            cax=axis1 = plt.subplot(222, sharey = ax1)
            # plt.setp(axis1.get_xticklabels(), visible=False)
            plt.setp(axis1.get_yticklabels(), visible=False)
            plt.tick_params(
                axis='y',          
                which='both',     
                right='off',     
                labelleft='off',
                labelright='off')
            plt.tick_params(
                axis='y',          
                which='minor',     
                left='off',
                right='off')
            plt.tick_params(
                axis='x',          
                which='both',     
                bottom='off',     
                labelbottom='off')
        elif x==3:
            cax=ax3 = plt.subplot(223, sharey = ax1)
            plt.tick_params(
                axis='y',          
                which='both',     
                right='off',     
                labelright='off')
            plt.tick_params(
                axis='y',          
                which='minor',     
                left='off')
            plt.tick_params(
                axis='x',          
                which='both',     
                top='off',     
                labeltop='off')
        elif x==4:
            cax=ax4 = plt.subplot(224, sharey = ax1)
            plt.tick_params(
                axis='y',          
                which='both',     
                right='off',     
                labelleft='off')
            plt.tick_params(
                axis='y',          
                which='minor',     
                left='off')
            plt.tick_params(
                axis='x',          
                which='both',     
                top='off',     
                labeltop='off')
            # plt.setp(ax4.get_yticklabels(), visible=False)
        print '\t init data..'
        dat.tauc = tauc_array[x-1]
        dat.init()
        dat.trueTimes *= 1e15
        # dat.Ein = 7e-17

        EnergyGap = main.GetEnergyGap(dat, trueI)

        edens=get_ed(dat,trueI,EnergyGap)[0]
        print 'max ed', edens[0,:].max()
        plt.semilogy(dat.trueTimes,edens[0,:]/1e6,'b-', dat.trueTimes, get_ed(dat, trueI, EnergyGap, False)[0][0,:]/1e6, 'k--')
        plt.ylim(ymin = 1e13)
        plt.xlim(-300,300)
        cax.xaxis.set_ticks([-150,0,150])
        cax.yaxis.set_ticks([1e14,1e16,1e18,1e20,1e22])
        for tl in cax.get_yticklabels():
            tl.set_color('b')

        props = dict(boxstyle='round', facecolor='wheat', alpha=0)
        cax.text(0.05, 0.95, r"$\tau_c=%s\ fs$"  % (dat.tauc*1e15), 
            transform=cax.transAxes, fontsize=14, verticalalignment='top', bbox=props,color="b")
        if x == 1:
            plt.ylabel(r'$\rho /cm^{-3}$', color="b")
        if x==3:
            plt.xlabel(r'$\tau /fs$')
        # plt.title(r"$\tau_c=%0.2e\ s,\ edmax = %0.2ecm^{-3}$"  % (dat.tauc,edens[0,:].max()/1e6), fontsize=16)
    # plt.subplot(223)
    # plt.plot(dat.trueTimes,np.trapz(edens,dat.trueRadii,axis=0) )
    # plt.title('Intensity at r=0')
    
    ax = axis1.twinx()
    ax.semilogy(dat.trueTimes,trueI[0,:]/1e4,'r-.')
    ax.set_ylim(ymin = 1e8)
    ax.yaxis.set_ticks([1e9,1e11,1e13,1e15])
    # plt.setp(ax.get_yticklabels(), visible=False)
    ax.set_xlim(-300,300)
    plt.tick_params(
                axis='y',          
                which='minor',     
                left='off',
                right='off')
    ax.set_ylabel('$I (W/cm^2)$', color='r')
    for tl in ax.get_yticklabels():
        tl.set_color('r')
    # plt.tick_params(
    #             axis='y',          
    #             which='both',     
    #             left='off',  
    #             labelleft='off')
    #x->t,y->density
    print '==> max i', trueI.max()

    # fig.savefig("foo.eps", format="eps")
    plt.show()
