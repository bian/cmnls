#!/bin/python
#coding=utf-8

import numpy as np
import numpy.ma as ma
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.io as sio
import scipy.special as sps
import sympy


"""
mpi几率/光强
"""

wpi_tmp = None
def _wpiDivI(I,dat):
    global wpi_tmp
    if wpi_tmp is None:
        wpi_tmp = dat.atomDensity*dat.mpiCoeff
    return wpi_tmp*np.power(I, dat.K-1)#m^-3*s^-1


h_=6.6260693e-34/(2*np.pi)

def _pi_keldysh(dat, I):
    # dat.w0=2*np.pi/(160e-15)#hz
    M=dat.Mass
    global h_
    if I==0: 
        _I = np.finfo(float).eps
    else:
        _I = I

    # print 'w0',dat.w0,'Ui',dat.Ui,'Ee',dat.Ee,'I',_I
    _I /= dat.factor_E2_S
    dgama=(dat.w0**2 *(M*dat.Ui)/dat.Ee**2)/_I
    # /405.82
    # print '\t\tgama',np.sqrt(dgama)
    gama1=dgama/(1.0+dgama)
    gama2=1.0 - gama1

    Kgama1=sps.ellipkm1(gama1)
    Egama1=sps.ellipe(gama1)
    Kgama2=sps.ellipkm1(gama2)
    Egama2=sps.ellipe(gama2)

    eks=(2/np.pi) * (dat.Ui/(h_*dat.w0)) * Egama2/np.sqrt(gama1)
    # alpha=np.pi * (Kgama2-Egama2) /Egama1
    beta=np.pi * (Kgama1-Egama1) /Egama2

    Cons=(2*dat.w0/(9*np.pi)) *\
        (dat.w0*M/(h_* np.sqrt( gama1)))**1.5 *\
        np.sqrt(np.pi*0.5/Kgama2)*\
        np.exp(- beta * int(eks+1))
        # np.exp(- beta * eks)
    # print "Cons", (dat.w0*M/(h_* np.sqrt( gama1)))**1.5 , np.sqrt(np.pi*0.5/Kgama2), exp(-alpha* int(eks+1))

    n_ = sympy.Symbol('n_')
    dawson_para =(np.pi/2)*sympy.sqrt( (n_ + 2*(int(eks+1)-eks)) / (Kgama2 * Egama2))
    # print dawsn
    Q= sympy.summation(
        sympy.exp(-n_*beta) *
        (-sympy.I*sympy.sqrt(sympy.pi)/2)*
        sympy.erf(sympy.I*dawson_para) *
        sympy.exp( - dawson_para**2) 
        ,(n_, 0, sympy.oo)
        )
    # print "I, gama, exp,q", I,np.sqrt(dgama), np.exp(-beta * int(eks+1)), Q.evalf()
    # print "alpha, x, a_c_x", beta, eks, beta*eks
    ret=Cons * Q.evalf()
    return ret

def pi_keldysh(dat, I):
    sz=I.shape
    ret=np.zeros(sz)
    for ki in range(sz[0]):
        for kj in range(sz[1]):
            ret[ki,kj] = _pi_keldysh(dat,I[ki,kj])
    return ret

def main():
    import main
    dat=main.dataPools()
    I=1e16 * 10**np.array([np.linspace(0, 3,100)])
    wpi = _wpiDivI(I,dat)*I
    keldysh_pi_rate = pi_keldysh(dat,I)
    plt.loglog(I[0]/1e4,wpi[0]/1e6, '-.',I[0]/1e4, keldysh_pi_rate[0]/1e6, 'r')
    plt.ylim(1e25, 1e38)
    plt.ylabel('$Rate (s^{-1}cm^{-3})$')
    plt.xlabel('$I (W/cm^2)$')
    plt.show()

if __name__ == '__main__':
    main()