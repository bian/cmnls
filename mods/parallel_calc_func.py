#!/bin/python
#coding=utf-8

from derive import derive_symmetric_fft_column, derive2_symmetric_fft_column, derive_fft, derive2_fft

import threading
# time consuming function
# 8s -> 5.38s
# 1st deriver 8s -> 6.5s delta_T = 1.5s
# 2nd deriver ditto


def calc_diffraction(data, field, dest,intensity, lock):
    if data.ctrl_diffraction :
        laplas_part1 = data.divRad * derive_symmetric_fft_column(field,data.rMax, start_index = 1)
        laplas_part2 = derive2_symmetric_fft_column(field,data.rMax, start_index = 1)

        laplas=laplas_part1 +laplas_part2
        if data.ctrl_T:
            stf=derive_fft(laplas,data.tMax)
            laplas -= (1/data.w0)*stf
        # lock.acquire()
        dest+=(0.5j/data.k)*laplas
        # lock.release()

    tmp_dest = None
    if data.ctrl_gvd:
        derive2T=derive2_fft(field,data.tMax, row=True)
        tmp_dest=-0.5j*data.k2*derive2T
    if data.ctrl_SF:
        Bb=intensity*field
        # TODO
        # ramanPart = Rintegral(data,intensity)*field
        # selfFocusingPart = 1j*data.k0*data.n2*(0.82*Bb + 0.18*ramanPart)
        selfFocusingPart = 1j*data.k0*data.n2*Bb
        if data.ctrl_T:
            DT=derive_fft(selfFocusingPart,data.tMax)
            selfFocusingPart += (1j/data.w0)*DT
        if tmp_dest is not None:
            tmp_dest += selfFocusingPart
        else:
            tmp_dest = selfFocusingPart
        
    if tmp_dest is not None:
        # lock.acquire()
        dest += tmp_dest
        # lock.release()


