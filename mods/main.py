#!/bin/python
#coding=utf-8

"""
计算NLSE的主程序，需要载入配置，无ui，使用管道链接
"""


import numpy as np
import numpy.ma as ma
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.io as sio

import sys
flush=sys.stdout.flush

from derive import *
from edensity import *

import pickle
# import multiprocessing
import threading

from initdata import varPool

import parallel_calc_func

if __name__=='__main__':
    arglen=len(sys.argv)
    if arglen!=2:
        print 'no specific cfg file, %d arguments' % arglen
        exit()
    print('Reading cfg file: %s' % sys.argv[1])
    flush()
    with open(sys.argv[1], 'rb') as cfgfile:
        varPool=pickle.load(cfgfile)
else:
    print('main.py in module mode')

class dataPools:
    global varPool
    tmpclass = locals()
    for key in varPool:
        tmpclass[key] = varPool[key][0]

    #Constants
    _speedLight    = 2.99792458e+8
    Me=9.109e-31
    Ee=1.6e-19
    e0=8.854e-12
    
    Ui *= Ee
    #Rayleigh length
    zf=np.pi*(wf**2)*n0/wavelength
    f=focusDepth+(zf**2)/focusDepth#m
    #print 'f=',f
    wa0=wf*sqrt(1+(focusDepth/zf)**2)#beam width

    tMax   = widths*taup
    dt     = tMax/Ntemporal

    Num_Z_Slice           = np.floor( (maxPropDist/dz) + 0.5 )

    NumFrameInterval           = np.floor( ((1.0*Num_Z_Slice)/NZSlice) + 0.5 )
    #画图参数
    numPlots = NZSlice + 1

    k0=2*np.pi/wavelength
    w0=k0*_speedLight
    k=k0*n0

    reduced_mass_rate = 0.64
    Mass = reduced_mass_rate * Me

    _critical_rou = w0**2 * e0 * Me/(Ee**2)
    critical_electron_density = n0**2 * _critical_rou

    rMax          = RWidth*wa0

    _PowerIn=Ein/(taup* sqrt(0.5*pi))
    # E_peak UNIT is (W/m2)^1/2
    E_peak=sqrt(2*_PowerIn/(pi*wa0**2))
    # Ipeak = Epeak**2
    #intensity, power, and energy
    #plottingFactor = 2.0*pi*(E0)*dt

    #form radius axis
    trueRadii=np.zeros((Nradial,1))
    dr=rMax/Nradial
    
    trueTimes   = np.zeros( (Ntemporal, 1 ))
    
    def __init__(self):
        self.init()
    def init(self):
         self.dampterm = self.w0*self.tauc
         self.rou=self.k * self.dampterm/(self.critical_electron_density * (1+self.dampterm**2 )  )
         # self.rou = 1.55e-22
         # self.rouDivUi=self.rou/self.Ui
         # self.trueRadii[0]=self.dr/2.0
         self.trueRadii[0]=np.finfo(float).eps
         for irad in xrange(1,self.Nradial): self.trueRadii[irad] =  self.trueRadii[irad-1]+self.dr
         self.divRad=np.dot(1/self.trueRadii,np.ones((1,self.Ntemporal)))
         self.plotr = self.trueRadii
         tvalue      = - ( self.Ntemporal / 2 )*self.dt
         for itime in xrange(self.Ntemporal):
            self.trueTimes[itime] = tvalue
            tvalue += self.dt
         self.plott = self.trueTimes

         self.gap_factor = self.Ee**2 / (2*self._speedLight*self.n0*self.e0*self.Mass* self.w0**2)
         # self.factor_E2_S = 0.5*self._speedLight*self.e0*self.n0
         print 'M:waist =' , self.wa0
         print 'M:Drude rou =' ,  self.rou
         print 'M:w0 =' , self.w0
         print 'M:critical electron density =' ,  self.critical_electron_density
         print 'M:dr =' , self.dr , 'dz =' ,  self.dz
         print 'M:damp term =', self.dampterm
         # print 'M:factor_E2_S =', self.factor_E2_S
         flush()

class savedDataPool:

    def __init__(self,data,initialPulse,eDensity):
        self.data=data
        self.distances=np.zeros((data.numPlots,1))
        self.energies=np.zeros((data.numPlots,1))
        self.eleDensity=np.zeros((data.Nradial,data.Ntemporal,data.numPlots))
        self.intensity=np.zeros((data.Nradial,data.Ntemporal,data.numPlots))
        self.fields=np.zeros((data.Nradial,data.Ntemporal,data.numPlots))*0j

        self.depositedI_dz=np.zeros((data.Nradial,data.Ntemporal,data.numPlots))

        #正在进行的
        self.curFigure=0
        self.field=initialPulse
        self.ed=eDensity
        self.depositedI = np.zeros(initialPulse.shape)
        self._calcAll(0)
    def pushSlice(self,newfield,eDensity, depositedI, step):
        self.field=newfield
        self.ed=eDensity
        self.depositedI = depositedI
        self.curFigure+=1

        #~ print('pushing..')
        #~ sys.stdout.flush()
        self._calcAll(step)

    def _calcAll(self,step):
        #根据field计算能量，强度
        intens=(abs(self.field))**2
        if self.curFigure > (self.data.numPlots-1):
            print( 'overflow..')
            flush()
            return
        #print(self.intensity[:,:,self.curFigure].shape,intens.shape)
        self.fields[:,:,self.curFigure]=self.field
        self.intensity[:,:,self.curFigure]=intens
        self.eleDensity[:,:,self.curFigure]=self.ed
        self.distances[self.curFigure]=step*self.data.dz
        
        self.depositedI_dz[:,:,self.curFigure]=self.depositedI
    #~ def saveAll(self):
        #~ print('->saving all data...')
        #~ sio.savemat('distances.mat',{'distances':self.distances})
        #~ sio.savemat('energies.mat',{'energies':self.energies})
        #~ sio.savemat('intensity.mat',{'intensity':self.intensity})
        #~ sio.savemat('eleDensity.mat',{'eleDensity':self.eleDensity})

def saveData(data1,data2):
    np.savez('./cfg/data/%s.npz' % sys.argv[1].split('/')[1], plotr=data1.plotr,plott=data1.plott,\
    distances=data2.distances,\
    energies=data2.energies,\
    intensity=data2.intensity,\
    eleDensity=data2.eleDensity,\
    depositedIdz = data2.depositedI_dz,\
    E_peak=data1.E_peak,\
    fields=data2.fields)
#mf=0


taud = 32e-15
taus = 12e-15

omegaSquare = taud**(-2) + taus**(-2)
raman = lambda x : omegaSquare*taus*np.exp(-x/taud)*np.sin(x/taus)
def Rintegral(data,Intensity):
    """
    retarded Raman-Kerr Effect
    """
    Rinteg = zeros(Intensity.shape)

    for ki in xrange(1,data.Ntemporal):
        #构造r*ki矩阵
        ramanMatrix = raman(data.trueTimes[ki] - data.trueTimes[:ki]).T
        ramanMatrixExpand = np.dot(np.ones((data.Nradial,1)),ramanMatrix)

        tss = ramanMatrixExpand * Intensity[:,:ki]

        Rinteg[:,ki] = np.trapz(tss , dx = data.dt ,axis = 1)
    return Rinteg


def GetEnergyGap(data, Intensity):
    return data.Ui + data.gap_factor * Intensity


def stepForward(data,field):
    dFieldDivdZ=zeros(field.shape)*0j
    lock = threading.Lock()
    intensity=abs(field)**2
    t = threading.Thread(target = parallel_calc_func.calc_diffraction, args=(data, field,dFieldDivdZ,intensity, lock))
    t.start()

    # parallel_calc_func.calc_diffraction(data, field,dFieldDivdZ, lock)
    # parallel_calc_func.calc_gvd(data, field,intensity, dFieldDivdZ, lock)

    plfactor = 0
    dDepositedE_div_dz = 0
    if data.ctrl_plasma_absorb:
        plfactor += 1
    if data.ctrl_plasma_defocusing:
        plfactor += 1j*data.dampterm

    EnergyGap = GetEnergyGap(data, intensity)
    eDensity, wpi_div_I=get_ed(data,intensity, EnergyGap)

    print 'D:%s,%s' % (intensity.max(), eDensity.max())
    # # print 'Imin',intensity.min(), 'ed min', eDensity.min()
    flush()

    tmp_dz = None
    if plfactor!=0:
        EDF=eDensity*field
        if data.ctrl_T:
            EDF -=(1/data.w0)*derive_fft(EDF,data.tMax)

        plPart=0.5*data.rou*EDF * plfactor
        tmp_dz = -plPart
        dDepositedE_div_dz += plPart

    if data.ctrl_pi:
        PIPart=0.5*EnergyGap*wpi_div_I*field
        if tmp_dz is not None:
            tmp_dz -= PIPart
        else:
            tmp_dz = -PIPart
        dDepositedE_div_dz += PIPart 

    dDepositedI_div_dz = abs(dDepositedE_div_dz)**2 * data.dz

    t.join()
    if tmp_dz is not None:
        dFieldDivdZ += tmp_dz
    destfield=field+dFieldDivdZ*data.dz
    return destfield,eDensity, dDepositedI_div_dz

"""
The unit of field is (W/m2)^1/2, not V/m
"""
def getGaussPulse(dat):
    rSquare=dat.trueRadii**2
    k1=-1/(dat.wa0**2) -0.5j*dat.k/dat.f
    k2=rSquare*k1
    field=np.dot(np.exp(k2),np.exp(- (dat.trueTimes/dat.taup)**2).T)*dat.E_peak
    return field

def checkInfNan(variable,name):
    test=np.sum(np.sum(np.isnan(variable)))
    if test>0:
        print('<- Detected NaN in %s!' % name)
        flush()
        return False
    else:
        return True
def main():
    #形成时间轴，径向轴
    dat=dataPools()
    #形成高斯脉冲
    pulse=getGaussPulse(dat)
    print('Cacl initial electron density..')
    flush()
    initialIntensity = abs(pulse)**2
    
    eDensity=get_ed(dat,initialIntensity, GetEnergyGap(dat,initialIntensity))[0]
    #目标存储对象
    print('Push initial data..')
    flush()
    destData=savedDataPool(dat,pulse,eDensity)

    #loop
    percentage=-1
    print( 'We will go through %d steps...' % dat.Num_Z_Slice)
    flush()
    for step in xrange(int(dat.Num_Z_Slice)):
        percentnow=np.floor(100.0*step/dat.Num_Z_Slice)
        if percentage!=percentnow:
            percentage=percentnow
            print('->%d' % (percentage+1))
            flush()
        pulse,eDensity, depositedI=stepForward(dat,pulse)
        #截取片段存储
        if np.mod(step,dat.NumFrameInterval)==0:
            #测试溢出
            if not checkInfNan(pulse,'field'): break
            destData.pushSlice(pulse, eDensity, depositedI, step)

    saveData(dat,destData)
    #print('Work finished!')
    #flush()

if __name__ == '__main__':
    main()