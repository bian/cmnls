#!/bin/python
#coding:utf-8

import numpy as np
import numpy
from numpy import *
import scipy as sp
import scipy
from scipy import fftpack
import matplotlib as mpl
import matplotlib.pyplot as plt

def deriver(y,dx, row = True):
    '''
    对每row求导
    row = True: 起码一行
    '''
#如果y是复数，则去除虚数部分，一般求导虚数部分没有意义
    sz=shape(y)
    dy=np.zeros(sz)*0j
    if row:
        dy[:,0]=(-3*y[:,0]+4*y[:,1]-y[:,2]) / (2*dx)
        for ki in xrange(1,sz[1]-1):
            dy[:,ki]=(-y[:,ki-1]+y[:,ki+1]) / (2*dx)
        dy[:,sz[1]-1]=(y[:,sz[1]-3]-4*y[:,sz[1]-2]+3*y[:,sz[1]-1]) / (2*dx)
    else:
        dy[0]=(-3*y[0]+4*y[1]-y[2]) / (2*dx)
        for ki in xrange(1,sz[0]-1):
            dy[ki]=(-y[ki-1]+y[ki+1]) / (2*dx)
        dy[sz[0]-1]=(y[sz[0]-3]-4*y[sz[0]-2]+3*y[sz[0]-1]) / (2*dx)        
    return dy

def _get_w0_matrix(shape, T, row = True):
    dw=2*np.pi/ T
    if row:
        _w0_row=np.zeros((1,shape[1]))*0j
        for ki in xrange(shape[1]):
            if ki<=shape[1]/2:
                _w0_row[0][ki]=dw*ki
            else :
                _w0_row[0][ki]=-dw*(shape[1]-ki)
        _w0_row = np.dot(np.ones((shape[0],1)),_w0_row)
        return _w0_row
    else:
        _w0_col=np.zeros(( shape[0], 1))*0j
        for ki in xrange(shape[0]):
            if ki<=shape[0]/2:
                _w0_col[ki]=dw*ki
            else :
                # print _w0_col.shape, ki
                _w0_col[ki]=-dw*(shape[0]-ki)
        _w0_col = np.dot(_w0_col, np.ones((1, shape[1])))
        return _w0_col



        
def derive_fft(y,T, row = True):
    '''
    傅立叶微分性质计算数值微分
    row=ture: 逐行计算,至少一行
    T:代表整个原函数x范围
    要求y两端趋于0
    '''
    sz=shape(y)
    w = _get_w0_matrix(sz, T, row)
    ax = 0
    if row: ax = 1
    return scipy.fftpack.ifft(1j*w*scipy.fftpack.fft(y, axis=ax), axis=ax)

def derive2_fft(y,T, row = True):
    sz=shape(y)
    w = _get_w0_matrix(sz, T, row)
    ax = 0
    if row: ax = 1
    return scipy.fftpack.ifft(-(w**2)*scipy.fftpack.fft(y, axis=ax), axis=ax)
"""
将数据上下对称后求导，再取一半数据
"""
def derive_symmetric_fft_column(y,T,start_index = 1):
    yud=np.vstack([y[::-1,:],y[start_index:]])
    yudD=derive_fft(yud, 2*T, False)
    
    return yudD[(y.shape[0]-start_index):]

def derive2_symmetric_fft_column(y,T,start_index = 1):
    yud=np.vstack([y[::-1,:],y[start_index:]])
    yudD=derive2_fft(yud, 2*T, False)
    
    return yudD[(y.shape[0]-start_index):]


if __name__ == '__main__':
    print "Important visual test code!!!"
    #guassion shape
    x=np.arange(-20,13)[:,np.newaxis]
    x=x/33.
    # x=x.T
    dx=x[1]-x[0]#0.0303..
    xT=x[-1]-x[0]
    gs = np.exp(-(x/0.12)**2)

    d=derive_fft(gs, xT, row=False)
    dgs = -2*x*gs/0.12**2
    ddgs = (-2.0/0.12**2)*(gs-2*(x**2)*gs/0.12**2)
    # print x
    plt.subplot(221)
    # x 为1,33
    plt.plot(x,gs,x,dgs,x,d.real)
    plt.title('gaussion, 1st derive')
    plt.subplot(222)
    d = derive2_fft(gs, xT, row=False)
    plt.plot(x,gs,x,ddgs,x,d)
    plt.title('gaussion, 2nd derive')

    x=np.arange(0,33)[:,np.newaxis]
    x=x/33.
    # x=x.T
    dx=x[1,0]-x[0,0]#0.0303..
    xT=x[-1,0]-x[0,0]
    gs = np.exp(-(x/0.12)**2)

    d = derive_symmetric_fft_column(gs, xT)
    print 'gs,fftd shape', gs.shape, d.shape
    dgs = -2*x*gs/0.12**2
    ddgs = (-2.0/0.12**2)*(gs-2*(x**2)*gs/0.12**2)
    print x.shape,gs.shape
    plt.subplot(223)
    plt.plot(x,gs,x,dgs,x,d.real)
    plt.title('half gaussion, 1st derive')
    plt.subplot(224)
    d = derive2_symmetric_fft_column(gs, xT)
    plt.plot(x,gs,x,ddgs,x,d)
    plt.title('half gaussion, 2st derive')

    plt.show()
    # assertTrue(eq(d, d_standard_gs(self.x, d=self.dx), diff =1))