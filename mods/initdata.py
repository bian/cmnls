# -*- coding: utf-8 -*-

#参数参考自burakov，jap，2007
varPool={\
    'Ntemporal': [1000,u'时间上分割次数'],
    'taup' : [100e-15,u'1/e最大值处的时间半宽 [s]'],
    'widths' : [20,u'时间坐标扩展倍数'],

    'maxPropDist'       : [150e-6,u'最大传播距离 [m]'],
    #don't change
    'dz'          : [1e-8,u'传播步进 [m]'],
    'NZSlice'           : [50,u'传播方向存储数据次数'],

    'n0':[1.45,u'材料折射率'],
    'wavelength':[800e-9,u'激光波长 [m]'],#m

    'k2':[361e-28,u'二阶gvd系数 [s^2/m]'],#s**2/m
    'n2':[2.48e-20,u'非线性折射率 [m^2/W]'],
    #~ 'rou':1.55e-22,
    'tauc':[1.27e-15,u'电子动量散射时间 [s]'],
    'Ui':[9,u'带宽 [eV]'],#eV
    'K':[6,u'同时吸收光子数目'],#多光子吸收
    'mpiCoeff':[1.5e-95,u'多光子电离系数'],
    'atomDensity':[2.1e+28,u'背景原子密度'],
    #~ 'w0Xtauc':3,
    'Ein':[1e-6,u'入射能量 [J]'],#j
    #~ 'fR':0.18,

    'focusDepth':[7.5e-5,u'聚焦深度 [m]'],#m
    #~ 'zf':5.7e-6,#m
    'wf':[0.9e-6,u'beam waist [m]'],#m

    'Nradial' :[500,u'径向分割次数'],#多少个点
    'RWidth'        : [20,u'径向扩展倍数'],#向外扩展多少个长度
    #控制效应开关
    'ctrl_SF':[1,u'自聚焦开关'],
    'ctrl_diffraction':[1,u'衍射开关'],
    'ctrl_T':[1,u'T算子开关'],
    'ctrl_gvd':[1,u'GVD开关'],
    'ctrl_plasma_absorb':[1,u'等离子体线性吸收开关'],
    'ctrl_plasma_defocusing':[1,u'等离子体散焦效应开关'],
    'ctrl_pi':[1,u'多光子电离吸收能量开关'],
    }
