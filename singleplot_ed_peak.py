import numpy as np
from numpy import ma
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable

from mpl_toolkits.mplot3d import axes3d
import sys

import plot_cl as pcl
import rplot_edensity as rped

cmaper = cm.Reds

def plot_density(axis, X, Y, plott, edens):
    ed = edens.max(1)
    Z = np.vstack([ed[::-1,:],ed[1:]])

    surf=axis.contourf(X,Y,Z,cmap=cmaper)
    axc=pcl.add_colorbar_axis(axis)
    plt.colorbar(surf, cax=axc)
    axis.set_xlabel('distance(um)')
    axis.set_ylabel('radius(um)')
    props = dict(boxstyle='round', facecolor='wheat', alpha=0)
    axis.text(0.05, 0.95, 'Electron density($1/cm^3$)', transform=axis.transAxes, fontsize=14, verticalalignment='top', bbox=props)
    return surf


def main():

    arglen=len(sys.argv)
    if arglen<2:
        print 'no specific dest file'
        exit()

    fig = plt.figure()
    ax = plt.gca()#fig.add_subplot(1, 1, 1)

    assert arglen-1 == 1

    X, Y, plott, dens =rped.parse_from_file(sys.argv[1])
    plot_density(ax, X, Y, plott, dens)

    ax.set_xlabel('distance(um)')
    ax.set_ylabel('radius(um)')
    plt.show()

if __name__ == '__main__':
    main()
