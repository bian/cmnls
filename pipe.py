
import time
import threading

pipedata = {}
curselcfg = ''
plotthread = None

exit_flag = False

def reset_cfg(cfgname):
    global pipedata
    pipedata[cfgname]={'paras':'%s\n' % cfgname+'='*40+'\n', 'I':[],'ED':[]}

def populate(cfgname, data):
    global pipedata
    if pipedata.has_key(cfgname):
        dic = pipedata[cfgname]
        dic['paras'] += data
def pushdata(cfgname, data):
    global pipedata
    if pipedata.has_key(cfgname):
        dic = pipedata[cfgname]
        sd = data.split(',')
        dic['I'].append(eval(sd[0]))
        dic['ED'].append(eval(sd[1]))

def plot_data(cfgname, plotfunc):
    global pipedata
    if pipedata.has_key(cfgname):
        dic = pipedata[cfgname]
        plotfunc(cfgname, dic['I'], dic['ED'])

def start_thread(plotfunc):
    global plotthread
    plotthread = threading.Thread(target = inthread_func, args=(plotfunc,))
    plotthread.start()

def inthread_func(plotfunc):
    global exit_flag
    while not exit_flag:
        if curselcfg!= None: 
            try:
                plot_data(curselcfg,plotfunc)
            except Exception, e:
                print 'plot thread error!', e   
        time.sleep(2)

if __name__ == '__main__':
    cfg = '2dd'
    reset_cfg(cfg)
    populate(cfg, 'dasdfas')
    pushdata(cfg, '2.32e90,232')
    print pipedata
