#!/usr/bin/env python
# -*- coding: utf-8 -*-


import wx
import numpy as np
from numpy import *
import scipy as sp
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import scipy.io as sio
import matplotlib.ticker as ticker

from mpl_toolkits.mplot3d import axes3d
import sys

import plot_cl as pcl

def main():
    arglen=len(sys.argv)
    if arglen!=2:
        print 'not specific dest file'
        return

    plotr, plott, distances, intensity, eleDensity,Fluence,depositedEnergyDensity= pcl.parse_variable_from_file(sys.argv[1])

    X, Y = np.meshgrid(distances*1e6,plotr*1e6)

    fig = plt.figure()
    # timeMin = float(sys.argv[2])*1e-15

    surf=plt.contour(X,Y,eleDensity/1e6,origin='lower', levels=[1e16,1e18,1e20],
        colors=('blue', 'green', 'r', (1,1,0), '#afeeee', '0.5'))
    plt.clabel(surf, fontsize=9, inline=1, fmt='%1.0e')
    plt.ylim(-8, 8)
    #~ orientation='horizontal', 
    plt.xlabel('distance/um')
    plt.ylabel('radius/um')
    plt.title('$50\ fs\ ed\ (1/cm^3)$')
    # formatter = ticker.FormatStrFormatter('%2.1e')
    plt.show()
 

if __name__ == '__main__':
    main()